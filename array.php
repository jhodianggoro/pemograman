<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Array</h1>
    <h3>Looping Array 1 Dimensi</h3>
    <div id="dimensi1"></div>
    <h3>Looping Array 2 Dimensi</h3>
    <div id="dimensi2"></div>
</body>
</html>
<script>
    const buah = ["apel", "jeruk", "anggur", "manggis", "salak", "durian"];
    let text = "";
    alert (buah.length)
    for (let i=0; i < buah.length; i++){
        text += buah [i] + "<br>";
    }
    document.getElementById("dimensi1").innerHTML = text;

    var input = [
                ["0001", "ucup", "lampung", "januari", "main bola"],
                ["0002", "asep", "jakarta", "februari", "bulu tangkis"],
                ["0003", "neneng", "kediri", "maret", "membaca"],
                ["0004", "suep", "bogor", "april", "menulis"]

    ]
    var jawaban1=" "
    var arrayLength = input.length;
    for (var i = 0; i < arrayLength; i++){
        jawaban1+="Nomor ID : "+input[i][0]+"<br>Nama Lengkap : "+input[i][1]+"<br>TTL : "+input[i][2]+" "+input[i][3]+"<br>Hobi : "+input[i][4]+"<br><br>"
    }
    document.getElementById("dimensi2").innerHTML = jawaban1
</script>