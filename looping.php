<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
    <h3>Contoh Hasil Looping</h3>
    <div id="contoh1"></div>
    <h3>Contoh Hasil Looping</h3>
    <div id="contoh2"></div>
</body>
</html>
<script>
    var jawabanContoh = ""
    for (var i = 1; i < 5; i++) {
        jawabanContoh += "ini adalah angka ke " + [i] + "<br>"

    }
    document.getElementById("contoh1").innerHTML = jawabanContoh

    var jawabanContoh = ""
    for (var i = 1; i < 5; i++) {
        x=i*5;
        jawabanContoh += "ini adalah angka kelipatan 5 : " + [x] + "<br>"

    }
    document.getElementById("contoh2").innerHTML = jawabanContoh
</script>